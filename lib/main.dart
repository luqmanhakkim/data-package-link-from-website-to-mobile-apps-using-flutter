import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

void main() => runApp(MyApp());

// Get JSON response
// Generate Dart class
// Create Object
// Call API
// Create Response Object
// Copy URL
// Convert
// Define where the variable should be. Is it should be after pressed or others

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  //Public variable. Can be call even out
  Todo todo = Todo();

  Future getResponse() async {
    try {
      Response response =
          await Dio().get("http://www.mocky.io/v2/5cd536272e0000630052753e");

      setState(() {
        todo = Todo.fromJson(response.data);
      });
    } catch (e) {
      print(e);
    }
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getResponse();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView.builder(

        //? used to compare between two condition
        //example: todo?.posts?.length = straight away assume it as null
        //however, if todo?.posts?.length ?? 0 = straight away the output will be given according to the value
        //?? operator works like if-else condition
          itemCount: todo?.posts?.length ?? 0,
          itemBuilder: (BuildContext context, int index) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(16.0),

                  //use image.network if the picture was obtained from web
                  child: Image.network('${todo.posts[index].image}',
                  width: 100,
                  height: 240,
                  fit: BoxFit.scaleDown),
                ),

                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('${todo.posts[index].name}'),
                      Text('${todo.posts[index].address}'),
                    ],
                  ),
                )
              ],
            );
          }),
    );
  }
}

//Convert file from JSON
//No need to declare the class of post since the list of post is included in Todo class
//Just declare Todo Class in just a string
//Function name need to be relate with what we are doing

Todo todoFromJson(String str) => Todo.fromJson(json.decode(str));

String todoToJson(Todo data) => json.encode(data.toJson());

class Todo {
  List<Post> posts;

  Todo({
    this.posts,
  });

  factory Todo.fromJson(Map<String, dynamic> json) => new Todo(
    posts: new List<Post>.from(json["posts"].map((x) => Post.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "posts": new List<dynamic>.from(posts.map((x) => x.toJson())),
  };
}

class Post {
  String image;
  String name;
  String address;

  Post({
    this.image,
    this.name,
    this.address,
  });

  factory Post.fromJson(Map<String, dynamic> json) => new Post(
    image: json["image"],
    name: json["name"],
    address: json["address"],
  );

  Map<String, dynamic> toJson() => {
    "image": image,
    "name": name,
    "address": address,
  };
}


//List<Todo> todoFromJson(String str) =>
  //  new List<Todo>.from(json.decode(str).map((x) => Todo.fromJson(x)));

//class Todo {
  //int userId;
  //int id;
  //String title;
  //String body;

  //Todo({
    //this.userId,
    //this.id,
    //this.title,
    //this.body,
 // });

  //factory Todo.fromJson(Map<String, dynamic> json) => new Todo(
    //    userId: json["userId"],
      //  id: json["id"],
        //title: json["title"],
        //body: json["body"],
      //);

  //Map<String, dynamic> toJson() => {
    //    "userId": userId,
      //  "id": id,
        //"title": title,
       // "body": body,
      //};
//}
